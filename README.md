# Numerical Computing with Python #

This project is based on the course "Practical Numerical Methods with Python" on Open edX.

https://openedx.seas.gwu.edu/courses/course-v1:MAE+MAE6286+2017/about

Project Structure:
<ol>
<li>Phugoid Model - Phugoid theory, Euler's methods, Runge-Kutta methods</li>
<li>Space and Time - Finite difference solutions of PDEs, Computational Fluid Dynamics</li>
<li>Riding the Wave - Convection problems, Burger's equation, Traffic-Flow model</li>
<li>Spreading Out - Diffusion problems (1-D and 2-D), Boundary conditions for implicit and explicit models</li>
<li>Relax and Hold Steady - Elliptic problems, Laplace and Poisson equations, Linear solvers for PDEs</li>
</ol>
